#include "rogue.h"

const int MAP_HEIGHT = 25;
const int MAP_WIDTH = 100;

Entity* player;
Tile** map;

/* room */

Room createRoom(int y, int x, int height, int width)
{
    Room newRoom;

    newRoom.pos.y = y;
    newRoom.pos.x = x;
    newRoom.height = height;
    newRoom.width = width;
    newRoom.center.y = y + (int)(height / 2);
    newRoom.center.x = x + (int)(width / 2);

    return newRoom;
}

void addRoomToMap(Room room)
{
    for (int y = room.pos.y; y < room.pos.y + room.height; y++)
    {
        for (int x = room.pos.x; x < room.pos.x + room.width; x++)
        {
            map[y][x].ch = '.';
            map[y][x].walkable = true;
            map[y][x].transparent = true;
        }
    }
}

void connectRoomCenters(Position centerOne, Position centerTwo)
{
    Position temp;
    temp.x = centerOne.x;
    temp.y = centerOne.y;

    while (true)
    {
        if (abs((temp.x - 1) - centerTwo.x) < abs(temp.x - centerTwo.x))
            temp.x--;
        else if (abs((temp.x + 1) - centerTwo.x) < abs(temp.x - centerTwo.x))
            temp.x++;
        else if (abs((temp.y + 1) - centerTwo.y) < abs(temp.y - centerTwo.y))
            temp.y++;
        else if (abs((temp.y - 1) - centerTwo.y) < abs(temp.y - centerTwo.y))
            temp.y--;
        else
            break;

        map[temp.y][temp.x].ch = '.';
        map[temp.y][temp.x].walkable = true;
        map[temp.y][temp.x].transparent = true;
    }
}



/* player */

Entity* createPlayer(Position start_pos)
{
    Entity* newPlayer = calloc(1, sizeof(Entity));

    newPlayer->pos.y = start_pos.y;
    newPlayer->pos.x = start_pos.x;
    newPlayer->ch = '@';
    newPlayer->color = COLOR_PAIR(VISIBLE_COLOR);

    return newPlayer;
}

void handleInput(int input)
{
    Position newPos = { player->pos.y, player->pos.x };

    switch(input)
    {
        //move up
        case 'e':
            newPos.y--;
            break;
        //move down
        case 'c':
            newPos.y++;
            break;
        //move left
        case 's':
            newPos.x--;
            break;
        //move right
        case 'f':
            newPos.x++;
            break;
        default:
            break;
    }

    movePlayer(newPos);
}

void movePlayer(Position newPos)
{
    if (map[newPos.y][newPos.x].walkable)
    {
        clearFOV(player);
        player->pos.y = newPos.y;
        player->pos.x = newPos.x;
        makeFOV(player);
    }
}

/* map */

Tile** createMapTiles(void)
{
    Tile** tiles = calloc(MAP_HEIGHT, sizeof(Tile*));

    for (int y = 0; y < MAP_HEIGHT; y++)
    {
        tiles[y] = calloc(MAP_WIDTH, sizeof(Tile));
        for (int x = 0; x < MAP_WIDTH; x++)
        {
            tiles[y][x].ch = '#';
            tiles[y][x].color = COLOR_PAIR(VISIBLE_COLOR);
            tiles[y][x].walkable = false;
            tiles[y][x].transparent = false;
            tiles[y][x].visible = false;
            tiles[y][x].seen = false;
        }
    }

    return tiles;
}

Position setupMap(void)
{
    int y, x, height, width, n_rooms;
    n_rooms =  (rand() % 11) + 5;
    Room* rooms = calloc(n_rooms, sizeof(Room));
    Position start_pos;

    for (int i = 0; i < n_rooms; i++)
    {
        y = (rand() % (MAP_HEIGHT - 10)) + 1;
        x = (rand() % (MAP_WIDTH - 20)) + 1;
        height = (rand() % 7) + 3;
        width = (rand() % 15) + 5;
        rooms[i] = createRoom(y, x, height, width);
        addRoomToMap(rooms[i]);

        if (i > 0)
        {
            connectRoomCenters(rooms[i-1].center, rooms[i].center);
        }
    }

    start_pos.y = rooms[0].center.y;
    start_pos.x = rooms[0].center.x;

    free(rooms);

    return start_pos;
}

void freeMap(void)
{
    for (int y = 0; y < MAP_HEIGHT; y++)
    {
        free(map[y]);
    }
    free(map);
}


/* fov */

void makeFOV(Entity* player)
{
    int y, x, distance;
    int RADIUS = 15;
    Position target;

    map[player->pos.y][player->pos.x].visible = true;
    map[player->pos.y][player->pos.x].seen = true;

    for (y = player->pos.y - RADIUS; y < player->pos.y + RADIUS; y++)
    {
        for (x = player->pos.x - RADIUS; x < player->pos.x + RADIUS; x++)
        {
            target.y = y;
            target.x = x;
            distance = getDistance(player->pos, target);

            if (distance < RADIUS)
            {
                if (isInMap(y, x) && lineOfSight(player->pos, target))
                {
                    map[y][x].visible = true;
                    map[y][x].seen = true;
                }
            }
        }
    }
}

void clearFOV(Entity* player)
{
    int y, x;
    int RADIUS = 15;

    for (y = player->pos.y - RADIUS; y < player->pos.y + RADIUS; y++)
    {
        for (x = player->pos.x - RADIUS; x < player->pos.x + RADIUS; x++)
        {
            if (isInMap(y, x))
                map[y][x].visible = false;
        }
    }
}

int getDistance(Position origin, Position target)
{
    double dy, dx;
    int distance;
    dx = target.x - origin.x;
    dy = target.y - origin.y;
    distance = floor(sqrt((dx * dx) + (dy * dy)));

    return distance;
}

bool isInMap(int y, int x)
{
    if ((0 < y && y < MAP_HEIGHT - 1) && (0 < x && x < MAP_WIDTH - 1))
    {
        return true;
    }
    
    return false;
}

bool lineOfSight(Position origin, Position target)
{
    int t, x, y, abs_delta_x, abs_delta_y, sign_x, sign_y, delta_x, delta_y;

    delta_x = origin.x - target.x;
    delta_y = origin.y - target.y;

    abs_delta_x = abs(delta_x);
    abs_delta_y = abs(delta_y);

    sign_x = getSign(delta_x);
    sign_y = getSign(delta_y);

    x = target.x;
    y = target.y;

    if (abs_delta_x > abs_delta_y)
    {
        t = abs_delta_y * 2 - abs_delta_x;

        do
        {
            if (t >= 0)
            {
                y += sign_y;
                t -= abs_delta_x * 2;
            }

            x += sign_x;
            t += abs_delta_y * 2;

            if (x == origin.x && y == origin.y)
            {
                return true;
            }
        }
        while (map[y][x].transparent);

        return false;
    }
    else
    {
        t = abs_delta_x * 2 - abs_delta_y;

        do
        {
            if (t >= 0)
            {
                x += sign_x;
                t -= abs_delta_y * 2;
            }

            y += sign_y;
            t += abs_delta_x * 2;

            if (x == origin.x && y == origin.y)
            {
                return true;
            }
        }
        while (map[y][x].transparent);

        return false;
    }
}

int getSign(int a)
{
    return (a < 0) ? -1 : 1;
}

/* engine */

bool cursesSetup(void)
{
    initscr();
    noecho();
    curs_set(0);

    if (has_colors())
    {
        start_color();

        init_pair(VISIBLE_COLOR, COLOR_WHITE, COLOR_BLACK);
        init_pair(SEEN_COLOR, COLOR_BLUE, COLOR_BLACK);
        
        return true;
    }
    else
    {
        mvprintw(20, 50, "Your system doesn't support color. Can't start game!");
        getch();
        return false;
    }
}

void gameLoop(void)
{
    int ch;

    makeFOV(player);
    drawEverything();

    while((ch = getch()))
    {
        if (ch == 'q')
        {
            break;
        }

        handleInput(ch);
        drawEverything();
    }
}

void closeGame(void)
{
    endwin();
    free(player);
}


/* draw */

void drawMap(void)
{
    for (int y = 0; y < MAP_HEIGHT; y++)
    {
        for (int x = 0; x < MAP_WIDTH; x++)
        {
            if (map[y][x].visible)
            {
                mvaddch(y, x, map[y][x].ch | map[y][x].color);
            }
            else if (map[y][x].seen)
            {
                mvaddch(y, x, map[y][x].ch | COLOR_PAIR(SEEN_COLOR));
            }
            else
            {
                mvaddch(y, x, ' ');
            }
        }
    }
}

void drawEntity(Entity* entity)
{
    mvaddch(entity->pos.y, entity->pos.x, entity->ch | entity->color);
}

void drawEverything(void)
{
    clear();
    drawMap();
    drawEntity(player);
}


/**    **/

int main(void)
{
	Position start_pos;
	bool compatibleTerminal;

	compatibleTerminal = cursesSetup();

	if (compatibleTerminal)
	{
		srand((unsigned int)time(NULL));

		map = createMapTiles();
		start_pos = setupMap();
		player = createPlayer(start_pos);

		gameLoop();

		closeGame();
	}
	else
	{
		endwin();
	}

	return 0;
}
